const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

const geometry = new THREE.BoxGeometry();
const material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
const cube = new THREE.Mesh( geometry, material );
scene.add( cube );

camera.position.z = 5;

const hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 0.6 );
hemiLight.color.setHSL( 0.6, 1, 0.6 );
hemiLight.groundColor.setHSL( 0.095, 1, 0.75 );
hemiLight.position.set( 0, 50, 0 );
scene.add( hemiLight );

const hemiLightHelper = new THREE.HemisphereLightHelper( hemiLight, 10 );
scene.add( hemiLightHelper );

const dirLight = new THREE.DirectionalLight( 0xffffff, 1 );
dirLight.color.setHSL( 0.1, 1, 0.95 );
dirLight.position.set( - 1, 1.75, 1 );
dirLight.position.multiplyScalar( 30 );
scene.add( dirLight );

dirLight.castShadow = true;

dirLight.shadow.mapSize.width = 2048;
dirLight.shadow.mapSize.height = 2048;

const d = 50;

dirLight.shadow.camera.left = - d;
dirLight.shadow.camera.right = d;
dirLight.shadow.camera.top = d;
dirLight.shadow.camera.bottom = - d;

dirLight.shadow.camera.far = 3500;
dirLight.shadow.bias = - 0.0001;

const dirLightHelper = new THREE.DirectionalLightHelper( dirLight, 10 );
scene.add( dirLightHelper );

const objloader = new THREE.OBJLoader();
const mtlloader = new THREE.MTLLoader();

mtlloader.load( 'chaussette.mtl', ( material ) => {
    
    objloader.setMaterials(material);
    objloader.load( 'chaussette.obj', ( object ) => {

        scene.add(object);

    });

});

function animate() {
    requestAnimationFrame( animate );

    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;

    renderer.render( scene, camera );
};

animate();

controls = new THREE.PointerLockControls( camera, document.body );

document.addEventListener( 'mousedown', () => {
    controls.lock();
});

scene.add(controls.getObject());

document.addEventListener( 'keydown', ( event ) => {
    const direction = new THREE.Vector3();
    controls.getDirection(direction);
    direction.normalize();
    switch (event.code) {
        case 'KeyW':
            camera.position.add(direction);
            break;
        case 'KeyA':
            direction.cross(new THREE.Vector3(0, -1, 0));
            camera.position.add(direction);
            break;
        case 'KeyD':
            direction.cross(new THREE.Vector3(0, 1, 0));
            camera.position.add(direction);
            break;
    }
});
